package com.humanbooster.worldcup2018consumer.exception;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class WorldCup2018ServerException extends RuntimeException {

    public WorldCup2018ServerException(Exception e) {
        super(e);
    }
}
