package com.humanbooster.worldcup2018consumer.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class KnockoutItem implements Serializable {

    private String name;
    private List<KnockoutMatchItem> matches;

    public KnockoutItem() {

    }

    public KnockoutItem(String name, List<KnockoutMatchItem> matches) {
        this.name = name;
        this.matches = matches;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<KnockoutMatchItem> getMatches() {
        return matches;
    }

    public void setMatches(List<KnockoutMatchItem> matches) {
        this.matches = matches;
    }
}
