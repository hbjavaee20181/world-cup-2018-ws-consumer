package com.humanbooster.worldcup2018consumer.service;

import com.google.gson.Gson;
import com.humanbooster.worldcup2018consumer.constant.ApiConstants;
import com.humanbooster.worldcup2018consumer.exception.WorldCup2018ServerException;
import com.humanbooster.worldcup2018consumer.model.WordCupJsonItem;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class WorldCupWebServiceConsumer {

    private static final Gson GSON = new Gson();

    private static final OkHttpClient CLIENT = new OkHttpClient();

    private static final WorldCupWebServiceConsumer CONSUMER = new WorldCupWebServiceConsumer();

    public static WorldCupWebServiceConsumer getInstance() {
        return CONSUMER;
    }

    private WorldCupWebServiceConsumer() {

    }

    public WordCupJsonItem getAllData() {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(ApiConstants.API_URL)
                .build();

        try {
            Response response = client.newCall(request).execute();

            String json = response.body().string();

            return GSON.fromJson(json, WordCupJsonItem.class);
        } catch (IOException e) {
            throw new WorldCup2018ServerException(e);
        }
    }
}
